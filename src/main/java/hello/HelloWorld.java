package hello;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Use YAML message from message.yml file if present, otherwise use default
 * YAML message embedded in the executable jar, or fallback to a hard-coded
 * last resort YAML message.
 *
 * The message.yml file must be in the same dir as the executable jar and
 * must contain a single YAML map entry similar to the following:
 *
 * 		message: your custom message here
 */
public class HelloWorld
{
	private static String YAML_MESSAGE_FILE = "message.yml";

	public static void main(String[] args) {
		InputStream in = null;
		try {
			File msg = new File(YAML_MESSAGE_FILE);
			if (msg.exists()) {
				in = new FileInputStream(msg);
			} else {
				in = HelloWorld.class.getResourceAsStream("/hello.yml");
				if (in == null) {
					byte[] buf = "message: Hello Last Resort Message World!".getBytes("UTF-8");
					in = new ByteArrayInputStream(buf);
				}
			}

			Greeter greeter = new Greeter(in);
			System.out.println(greeter.sayHello());

		// Placate Java's checked exception handling fiasco with something
		// semi informative
		} catch (FileNotFoundException e) {
			System.err.println("YAML loading failed. Reason: " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			System.err.println("String encoding failed. Reason: " + e.getMessage());
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					System.err.println("Unable to close InputStream. Reason: " + e.getMessage());
				}
			}
		}
	}
}
