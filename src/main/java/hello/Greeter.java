package hello;

import java.io.InputStream;

public class Greeter
{
	public static String ERROR_MESSAGE = "[ERROR] Unable to get message";

	private InputStream in;

	public Greeter(InputStream stream) {
		in = stream;
	}

	public String sayHello() {
		DataStore yaml = new YamlStore(in);
		String rv = yaml.getString("message");

		return rv != null ? rv : ERROR_MESSAGE;
	}
}
