package hello;

import java.io.InputStream;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class YamlStore implements DataStore
{
	private InputStream in;

	public YamlStore(InputStream stream) {
		in = stream;
	}

	@SuppressWarnings("unchecked")
	public String getString(String key) {
		Yaml yaml = new Yaml();
		Map<String, String> msg = (Map<String, String>) yaml.load(in);

		return msg.get(key);
	}
}
