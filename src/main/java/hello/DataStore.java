package hello;

public interface DataStore
{
	public String getString(String key);
}
