package hello;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import org.junit.Test;
import static org.junit.Assert.assertThat;

import static org.hamcrest.CoreMatchers.is;

public class GreeterTest
{
	@Test
	public void getsStringForValidMessage() throws UnsupportedEncodingException {
		String msg = "Hello Test World!";
		byte[] buf = String.format("message: %s", msg).getBytes("UTF-8");
		InputStream in = new ByteArrayInputStream(buf);

		Greeter g = new Greeter(in);
		assertThat(g.sayHello(), is(msg));
	}

	@Test
	public void getsErrorStringForInvalidMessage() throws UnsupportedEncodingException {
		byte[] buf = "invalid: some bogus message".getBytes("UTF-8");
		InputStream in = new ByteArrayInputStream(buf);

		Greeter g = new Greeter(in);
		assertThat(g.sayHello(), is(Greeter.ERROR_MESSAGE));
	}
}
