package hello;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.assertThat;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

public class YamlStoreTest
{
	@Test
	public void getsStringForValidMessageKey() throws UnsupportedEncodingException {
		String msg = "Hello Test World!";
		byte[] buf = String.format("message: %s", msg).getBytes("UTF-8");
		InputStream in = new ByteArrayInputStream(buf);

		DataStore yaml = new YamlStore(in);
		assertThat(yaml.getString("message"), is(msg));
	}

	@Test
	public void getsNullForInvalidMessageKey() throws UnsupportedEncodingException {
		String msg = "Hello Test World!";
		byte[] buf = String.format("message: %s", msg).getBytes("UTF-8");
		InputStream in = new ByteArrayInputStream(buf);

		DataStore yaml = new YamlStore(in);
		assertThat(yaml.getString("invalid"), is(nullValue()));
	}

	@Ignore("Demo how to ignore a test")
	@Test
	public void testSanity() {
		assertThat(1, is(1));
	}
}
