# Overcomplicated Java HelloWorld

This demo project shows off some of the benefits of the [Gradle](http://gradle.org/)
open source build automation tool. Although it is a [Rube Goldberg](https://en.wikipedia.org/wiki/Rube_Goldberg)
style HelloWorld app, it demonstrates how gradle enables one to easily manage a
project and build an executable, all-in-one jar from either the command line or
a gradle aware IDE.

## Why Gradle?

Gradle makes it easy to develop and manage Java projects. Gradle's standardized project
layout, reporting capabilities, and extensibility (e.g. - the [FindBugs](http://findbugs.sourceforge.net/)
based static code analysis plugin) makes it a great tool for collaborating in small or large
development teams.

Gradle, via the `gradlew` gradle wrapper, is able to download itself and all project
dependencies so that you don't have to waste time updating your development environment
to match the original developers environment. Gradle only requires that your system has a
functioning [JDK 6](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
or higher installation.

Gradle builds upon the time-tested capabilities of [Ant](http://ant.apache.org/),
[Ivy](http://ant.apache.org/ivy/), and [Maven](http://maven.apache.org/) while avoiding
the tediousness of developing build scripts in XML. In contrast, gradle leverages the
[Groovy](http://groovy-lang.org/) language for writing build scripts that have full
access to the underlying Java runtime and the capabilities of Ant, Ivy, and Maven.
Gradle build scripts like this project's [build.gradle](https://bitbucket.org/jonforums/hello/src/851db1a6246899d25426139bfa2f2cbdb277fe2f/build.gradle?at=master)
are much easier to understand, maintain, and share that XML-based scripts or proprietary
IDE build tooling.

Gradle also enables other developers to quickly build your project without requiring them
to download, install, configure, and use your IDE version. Using your IDE's build system
is often convenient, but using it usually turns out to be a poor decision. A much better
strategy is to automate your project using gradle and take advantage of IDE's such as
[IntelliJ IDEA](https://www.jetbrains.com/idea/download/) that support gradle
[out of the box](https://www.jetbrains.com/idea/features/build_tools.html) by enabling
developers to [import gradle projects](https://www.youtube.com/watch?v=w7AKgaP_EmI).

## Build Instructions

By using gradle, specifically the `gradlew` gradle wrapper, one can build this project
by simply cloning this repository, changing to the directory containing the `build.gradle`
build script, and running `gradlew clean build` from your shell.

Under the generated `build` directory tree, you will find a tested, all-in-one (aka FatJar)
executable jar containing all project dependencies in `build/libs`, a browser viewable
report of the project tests in `build/reports/tests/index.html`, and project source
archives under `build/distributions`.

## Executing the Application

After building the executable jar, open a shell, and experiment similar to:

~~~ console
C:\Users\Jon\Documents\JavaDev\projects\hello>cd build\libs

# show default greeting using YAML messaged embedded in jar
C:\Users\Jon\Documents\JavaDev\projects\hello\build\libs>java -jar hello-0.5.0.jar
Hello Embedded YAML in a jar World!

# create a message.yml file with the a single line of YAML map content
# similar to the following
C:\Users\Jon\Documents\JavaDev\projects\hello\build\libs>dir
...
04/23/2015  02:35 PM    <DIR>          .
04/23/2015  02:35 PM    <DIR>          ..
04/23/2015  02:32 PM           268,317 hello-0.5.0.jar
04/23/2015  02:35 PM                54 message.yml

C:\Users\Jon\Documents\JavaDev\projects\hello\build\libs>type message.yml
message: Hello Custom YAML from External File World!

# show custom YAML message from message.yml
C:\Users\Jon\Documents\JavaDev\projects\hello\build\libs>java -jar hello-0.5.0.jar
Hello Custom YAML from External File World!

# clean the build
C:\Users\Jon\Documents\JavaDev\projects\hello\build\libs>cd ..\..

C:\Users\Jon\Documents\JavaDev\projects\hello>gradlew clean
:clean

BUILD SUCCESSFUL

Total time: 3.027 secs
~~~

## Common Gradle Commands

For all of the following examples, passing `-q` will cause gradle to behave more
quietly and log errors only. All examples use the `gradlew` gradle wrapper, but if
you already have gradle installed, you can replace `gradlew` with `gradle`.

* `gradlew build` - Assembles and tests this project
* `gradlew clean` - Deletes the build directory
* `gradlew jar` - Assembles a jar archive containing the main classes
* `gradlew javadoc` - Generates Javadoc API documentation for the main source code
* `gradlew dependencies` - Displays all dependencies declared in the root project
* `gradlew tasks` - Displays the tasks runnable from the root project
* `gradlew test` - Runs the unit tests
